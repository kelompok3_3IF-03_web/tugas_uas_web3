-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2019 at 04:59 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nodejs`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `category`, `author`, `publisher`, `createdAt`, `updatedAt`) VALUES
(1, 'KOLABORASI MACRO EXCEL DAN ACCESS UNTUK MEMBUAT APLIKASI PENGGAJIAN', 73416, 'Computer', 'Yudhy Wicaksono & Solusi Kantor', 'GRAMEDIA', '2019-07-17 00:00:00', '2019-07-17 00:00:00'),
(2, 'MUDAH MEMBUAT APLIKASI PEMASARAN DIGITAL 360 DERAJAT', 57776, 'Computer', 'Ridwan Sanjaya, dkk.', 'GRAMEDIA', '2019-07-17 05:41:02', '2019-07-17 05:41:02'),
(3, 'CISCO Kung Fu: Jurus-Jurus Routing', 57200, 'Computer', 'Aristo', 'Jasakom', '2019-07-17 05:45:00', '2019-07-17 05:46:58'),
(4, 'Rumus Sakti Fisika SMP MTs Kelas 7,8,9', 23375, 'Science', 'Tutor Dunia Cerdas', 'Dunia Cerdas', '2019-07-17 05:49:40', '2019-07-17 05:49:40'),
(5, 'Buku Paket: Belajar Sains untuk Anak Pintar', 59500, 'Science', 'Tim Penerbit', 'CIF', '2019-07-17 05:50:35', '2019-07-17 05:50:35'),
(6, 'Buku Saku Biologi SMA', 19550, 'Science', 'Andri Nurdiansyah, S.Pd. & Rini Nuraeni, M.Si.', 'Kaifa', '2019-07-17 05:51:39', '2019-07-17 05:51:39'),
(7, 'Majalah Rakyat Merdeka Inspiring Achievement Vol. 10', 36000, 'Magazine', 'Tim Redaksi', 'RM BOOKS', '2019-07-17 06:57:19', '2019-07-17 06:57:19'),
(8, 'RE:ON COMICS VOL. 33 PERIODICAL COMICS COMPILATION', 34000, 'Magazine', 're:ON', 'Buku Pintar Indonesia', '2019-07-17 07:04:51', '2019-07-17 10:17:25'),
(9, 'Zeros Familiar', 56100, 'Manga', 'Yamaguchi Noboru', 'Shining Rose Media', '2019-07-17 14:10:16', '2019-07-17 14:10:16'),
(10, 'Detektif Conan 95', 21250, 'Manga', 'Aoyama Gosho', 'Elex Media Komputindo', '2019-07-17 14:13:28', '2019-07-17 14:13:28'),
(11, 'Dragon Ball Super Vol. 2', 34000, 'Manga', 'Akita Toriyama', 'Elex Media Komputindo', '2019-07-17 14:19:47', '2019-07-17 14:19:47'),
(12, 'Komikus Shojo Nozaki 10', 21250, 'Manga', 'Izumi Tsubaki', 'Elex Media Komputindo', '2019-07-17 14:22:52', '2019-07-17 14:23:08'),
(13, 'Dragon Ball Super Vol. 3', 34000, 'Manga', 'Akita Toriyama', 'Elex Media Komputindo', '2019-07-17 14:24:35', '2019-07-17 14:24:35'),
(14, 'Tokyo Ghoul : Re 04', 46750, 'Manga', 'Sui Ishida', 'M&C', '2019-07-17 14:42:37', '2019-07-17 14:42:37'),
(15, 'Hacking Aplikasi Web : Uncensored', 61600, 'Computer', 'Efvy Zam', 'Jasakom', '2019-07-17 14:44:14', '2019-07-17 14:44:14'),
(16, '41 Script PHP Siap Pakai', 83600, 'Computer', 'Yosef Murya', 'Jasakom', '2019-07-17 14:44:42', '2019-07-17 14:44:42'),
(17, 'Panduan Merakit Hardware PC', 45760, 'Computer', 'Indra Susanto', 'Jasakom', '2019-07-17 14:46:04', '2019-07-17 14:46:04'),
(18, 'Kali Linux : 300% Attack', 57200, 'Computer', 'Fachrizal Oktavian', 'Jasakom', '2019-07-17 14:54:18', '2019-07-17 14:54:18'),
(19, 'Mikrotik Kung Fu Kitab 3 Edisi 2016', 74800, 'Computer', 'Rendra Towidjojo', 'Jasakom', '2019-07-17 14:55:14', '2019-07-17 14:55:14'),
(20, 'Buku Sakti Pemrograman Web Seri PHP', 41225, 'Computer', 'Mundzir Mf', 'Start Up', '2019-07-17 14:55:59', '2019-07-17 14:55:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `roles`, `createdAt`, `updatedAt`) VALUES
(1, 'fazril', 'fazril012@gmail.com', '$2a$10$m1HVYXaWsBIcNmh/VU.LHO1w62niCtQUEEZupQS7oCmBoxnNUdgxu', 'admin', '2019-07-17 06:16:30', '2019-07-17 06:16:30'),
(2, 'kelompok3', 'kelompok3@gmail.com', '$2a$10$T6iYBbx5Mwss34tWYNQ8oOv/UBEApAmSWe/3zWgzXq9jr7E7jQSy6', 'admin', '2019-07-17 06:19:49', '2019-07-17 06:19:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
