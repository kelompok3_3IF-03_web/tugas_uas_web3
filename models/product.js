const Sequelize = require('sequelize');

const sequelize = require('../configs/sequelize');

class Product extends Sequelize.Model {}

Product.init({
  name: Sequelize.STRING,
  price: Sequelize.INTEGER,
  category: Sequelize.STRING,
  author: Sequelize.STRING,
  publisher: Sequelize.STRING
}, { sequelize, modelName: 'product' });

module.exports = Product;