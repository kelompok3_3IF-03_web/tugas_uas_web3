const express = require('express');

const CartController = require('../controllers/cart');

const router = express.Router();

const auth = require('../configs/auth');

router.get('/',  CartController.getAll);

router.post('/addCart', auth.verifyToken, CartController.postAddCart);

module.exports = router;