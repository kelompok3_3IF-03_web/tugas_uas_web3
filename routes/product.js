const express = require('express');

const router = express.Router();

const productController = require('../controllers/product');
const auth =require('../configs/auth');

router.get('/books', productController.getAllProduct);
router.get('/books/detail/:id', productController.getOneProduct);

router.post('/admin/books', auth.verifyToken, productController.postProduct);
router.put('/admin/books/:id', auth.verifyToken, productController.putProduct);
router.delete('/admin/books/:id', auth.verifyToken, productController.deleteProduct);

module.exports = router;