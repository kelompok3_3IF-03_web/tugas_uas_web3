const Product = require('../models/product');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config();

module.exports.getAllProduct = (req, res) => {
	Product.findAll({
		id: req.params.id
	}, {
		where: {
			id: req.params.id
		}
	}).then((product) => {
		res.json(product);
	}).catch((error) => {
		throw error;
	})
}

module.exports.getOneProduct = (req, res) => {
	Product.findOne({
		where: {
			id: req.params.id
		}
	}).then((product) => {
		res.json(product);
	}).catch((error) => {
		throw error;
	})
}

module.exports.postProduct = (req, res) => {
	jwt.verify(req.token, process.env.SECRETKEY, (err, authData) =>{
		if(err){
			res.sendStatus(403)
		} else {
			if(authData['roles'] == 'admin') {
				Product.create({
					name: req.body.name,
					price: req.body.price,
					category: req.body.category,
					author: req.body.author,
					publisher: req.body.publisher
				}).then((product) => {
					res.json({
						message: 'Insert Data Success',
						authData: authData,
						product
					});
				})
			} else {
				res.sendStatus(403)
			}
		}
	})
}

module.exports.putProduct = (req, res) => {
	jwt.verify(req.token, process.env.SECRETKEY, (err, authData) =>{
		if(err){
			res.sendStatus(403)
		} else {
			if(authData['roles'] == 'admin') {
				Product.update({
					name: req.body.name,
					price: req.body.price,
					category: req.body.category,
					author: req.body.author,
					publisher: req.body.publisher
				}, {
					where: {
						id: req.params.id
					}
				}).then((product) => {
					res.json({
						message: 'Data has been updated',
						authData: authData
					});
				})
			} else {
				res.sendStatus(403)
			}
		}
	})
}

module.exports.deleteProduct = (req, res) => {
	jwt.verify(req.token, process.env.SECRETKEY, (err, authData) =>{
		if(err){
			res.sendStatus(403)
		} else {
			if(authData['roles'] == 'admin') {
				Product.destroy({
					where: {
						id: req.params.id
					}
				}).then((product) => {
					res.json({
						message: 'Data has been deleted',
						authData: authData
					});
				})
			} else {
				res.sendStatus(403)
			}
		}
	})
}