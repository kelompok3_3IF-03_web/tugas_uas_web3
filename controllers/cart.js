const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

const Cart = require('../models/cart');
module.exports.getAll = (req, res) =>{
	Cart.findAll().then(Cart=> {
		res.json(Cart);
	}).catch((error)=>{
		console.log(error);
	});
}

module.exports.postAddCart = (req, res) =>{
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
				res.sendStatus(403);
		}else{
			if (authData['roles'] == "user") {
				Cart.create({
						userId: authData['id'],
						bookId : req.body.bookId
				})
				.then(Cart => {
						res.json({
							message: 'Success to add cart',
							authData: authData,
							Cart
						});
				});
			} else {
				res.sendStatus(403);
			}
		}
	});
}